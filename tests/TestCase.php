<?php

namespace Karls\ErrorHandling\Tests;

use Karls\ErrorHandling\ErrorHandlingServiceProvider;

class TestCase extends \Tests\TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            ErrorHandlingServiceProvider::class,
        ];
    }
}
