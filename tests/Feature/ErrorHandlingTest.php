<?php

namespace Karls\ErrorHandling\Tests\Feature;

use Illuminate\Support\Facades\Route;
use Karls\Core\Tests\TestCase;
use Karls\ErrorHandling\Exceptions\CoreException;
use Karls\ErrorHandling\Exceptions\ErrorCode;
use Karls\ErrorHandling\Http\Requests\TesterRequest;
use Karls\ErrorHandling\Http\Resources\TesterResource;
use Karls\ErrorHandling\Models\Tester;
use Symfony\Component\HttpFoundation\Response;

class ErrorHandlingTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Route::get('testCoreException', function () {
            throw new CoreException(ErrorCode::VALIDATION_SAME);
        });

        Route::get('testResourceReturn', function () {
            return new TesterResource(Tester::make(['id' => 7]));
        });

        Route::get('testFailedRequestValidation', function (TesterRequest $request) {
            return new TesterResource($request->validated());
        });
    }

    public function test_throwing_core_exception_in_controller()
    {
        $response = $this->get('/testCoreException');
        $response
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'code',
                'message',
                'details' => []
            ])
            ->assertJson([
                'code' => ErrorCode::VALIDATION_SAME,
                'message' => ErrorCode::message(ErrorCode::VALIDATION_SAME)
            ]);
    }

    public function test_return_resource()
    {
        $response = $this->get('/testResourceReturn');
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data',
            ])
            ->assertJson([
                'data' => [
                    'id' => 7
                ]
            ]);
    }

    public function test_failed_request_validation()
    {
        $response = $this->get('/testFailedRequestValidation');
        $response
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'code',
                'message',
                'details' => [
                    'id'
                ]
            ])
            ->assertJson([
                'code' => ErrorCode::VALIDATION_FAILED,
                'message' => ErrorCode::message(ErrorCode::VALIDATION_FAILED),
                'details' => [
                    'id' => [
                        'code' => ErrorCode::VALIDATION_REQUIRED,
                        'message' => 'The id field is required.'
                    ]
                ]
            ]);
    }
}
