<?php


namespace Karls\ErrorHandling\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Throwable;

class CoreException extends Exception
{
    public const DEBUG = 'debug';
    public const INFO = 'info';
    public const WARNING = 'warning';
    public const ERROR = 'error';
    public const CRITICAL = 'critical';
    public const ALERT = 'alert';
    public const EMERGENCY = 'emergency';

    public const LEVEL_SCALE = [
        self::DEBUG => 1,
        self::INFO => 2,
        self::WARNING => 3,
        self::ERROR => 4,
        self::CRITICAL => 5,
        self::ALERT => 5,
        self::EMERGENCY => 5,
    ];

    /**
     * ReturnException constructor.
     *
     * @param int $code
     * @param string|null $level Log level for rollbar
     * @param array $extra Extra array data, to show in log
     * @param array $details
     * @param ?string $message fallback Message
     * @param Throwable|null $previous Previously thrown Exception
     * @param bool $logMe
     */
    public function __construct(
        int $code = 0,
        protected ?string $level = self::ERROR,
        protected array $details = [],
        protected array $extra = [],
        ?string $message = null,
        Throwable $previous = null,
        protected bool $logMe = true,
    )
    {
        $previous && $this->addPrevious($previous);
        $message = ErrorCode::message($code, $message ?? $previous?->getMessage());
        parent::__construct($message, $code, $previous);
    }

    protected function addPrevious(Throwable $prev): void
    {
        $maxLevel = max(self::LEVEL_SCALE[$this->level],
            ($prev instanceof CoreException)
                ? self::LEVEL_SCALE[$prev->level]
                : null);
        $this->level = array_flip(self::LEVEL_SCALE)[$maxLevel];

        $errorStack = [
            'message' => $prev->getMessage(),
            'code' => $prev->getCode(),
        ];
        if ($prev instanceof CoreException) {
            $errorStack += [
                'extra' => $prev->getExtra(),
                'level' => $prev->level,
            ];
            $prev->logMe = false;
            $this->extra['errorStack'] = $prev->errorStack ?? [];
        }

        $this->extra['errorStack'][] = $errorStack;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }

    public function report()
    {
        /* $this->logMe && Log::channel('api')->{$this->level}(
             $this->getMessage(),
             ['code' => $this->getCode()] + $this->getExtra()
         );*/

        $this->logMe = false;
    }

    /**
     * Returns array with Error Code and its specified Message
     */
    public function render(): JsonResponse
    {
        return errorResponse(
            $this->getCode(),
            $this->getMessage(),
            $this->getDetails());
    }

    public function getDetails(): array
    {
        return $this->details;
    }
}
