<?php

namespace Karls\ErrorHandling\Exceptions;

class ErrorCode
{
    public const UNKNOWN = 999999;
    /**
     * HTTP Errors
     * All HTTP Errors start with 10xxx
     */
    public const HTTP_CONTINUE = 10100;
    public const HTTP_SWITCHING_PROTOCOLS = 10101;
    public const HTTP_OK = 10200;
    public const HTTP_CREATED = 10201;
    public const HTTP_ACCEPTED = 10202;
    public const HTTP_NON_AUTHORITATIVE_INFORMATION = 10203;
    public const HTTP_NO_CONTENT = 10204;
    public const HTTP_RESET_CONTENT = 10205;
    public const HTTP_PARTIAL_CONTENT = 10206;
    public const HTTP_MULTIPLE_CHOICES = 10300;
    public const HTTP_MOVED_PERMANENTLY = 10301;
    public const HTTP_MOVED_TEMPORARILY = 10302;
    public const HTTP_SEE_OTHER = 10303;
    public const HTTP_NOT_MODIFIED = 10304;
    public const HTTP_USE_PROXY = 10305;
    public const HTTP_BAD_REQUEST = 10400;
    public const HTTP_UNAUTHORIZED = 10401;
    public const HTTP_PAYMENT_REQUIRED = 10402;
    public const HTTP_FORBIDDEN = 10403;
    public const HTTP_NOT_FOUND = 10404;
    public const HTTP_METHOD_NOT_ALLOWED = 10405;
    public const HTTP_NOT_ACCEPTABLE = 10406;
    public const HTTP_PROXY_AUTHENTICATION_REQUIRED = 10407;
    public const HTTP_REQUEST_TIME_OUT = 10408;
    public const HTTP_CONFLICT = 10409;
    public const HTTP_GONE = 10410;
    public const HTTP_LENGTH_REQUIRED = 10411;
    public const HTTP_PRECONDITION_FAILED = 10412;
    public const HTTP_REQUEST_ENTITY_TOO_LARGE = 10413;
    public const HTTP_REQUEST_URI_TOO_LARGE = 10414;
    public const HTTP_UNSUPPORTED_MEDIA_TYPE = 10415;
    public const HTTP_INTERNAL_SERVER_ERROR = 10500;
    public const HTTP_NOT_IMPLEMENTED = 10501;
    public const HTTP_BAD_GATEWAY = 10502;
    public const HTTP_SERVICE_UNAVAILABLE = 10503;
    public const HTTP_GATEWAY_TIME_OUT = 10504;
    public const HTTP_HTTP_VERSION_NOT_SUPPORTED = 10505;

    /**
     * Validation Errors
     * All Validation Errors start with 30xxx
     */
    public const VALIDATION_FAILED = 30000;
    public const VALIDATION_ACCEPTED = 30010;
    public const VALIDATION_ACTIVE_URL = 30020;
    public const VALIDATION_AFTER = 30030;
    public const VALIDATION_AFTER_OR_EQUAL = 30040;
    public const VALIDATION_ALPHA = 30050;
    public const VALIDATION_ALPHA_DASH = 30060;
    public const VALIDATION_ALPHA_NUM = 30070;
    public const VALIDATION_ARRAY = 30080;
    public const VALIDATION_BEFORE = 30090;
    public const VALIDATION_BEFORE_OR_EQUAL = 30100;
    public const VALIDATION_BETWEEN_NUMERIC = 30111;
    public const VALIDATION_BETWEEN_FILE = 30112;
    public const VALIDATION_BETWEEN_STRING = 30113;
    public const VALIDATION_BETWEEN_ARRAY = 30114;
    public const VALIDATION_BOOLEAN = 30120;
    public const VALIDATION_CONFIRMED = 30130;
    public const VALIDATION_DATE = 30140;
    public const VALIDATION_DATE_EQUALS = 30150;
    public const VALIDATION_DATE_FORMAT = 30160;
    public const VALIDATION_DIFFERENT = 30170;
    public const VALIDATION_DIGITS = 30180;
    public const VALIDATION_DIGITS_BETWEEN = 30190;
    public const VALIDATION_DIMENSIONS = 30200;
    public const VALIDATION_DISTINCT = 30210;
    public const VALIDATION_EMAIL = 30220;
    public const VALIDATION_ENDS_WITH = 30230;
    public const VALIDATION_EXISTS = 30240;
    public const VALIDATION_FILE = 30250;
    public const VALIDATION_FILLED = 30260;
    public const VALIDATION_GT_NUMERIC = 30271;
    public const VALIDATION_GT_FILE = 30272;
    public const VALIDATION_GT_STRING = 30273;
    public const VALIDATION_GT_ARRAY = 30274;
    public const VALIDATION_GTE_NUMERIC = 30281;
    public const VALIDATION_GTE_FILE = 30282;
    public const VALIDATION_GTE_STRING = 30283;
    public const VALIDATION_GTE_ARRAY = 30284;
    public const VALIDATION_IMAGE = 30290;
    public const VALIDATION_IN = 30300;
    public const VALIDATION_IN_ARRAY = 30310;
    public const VALIDATION_INTEGER = 30320;
    public const VALIDATION_IP = 30330;
    public const VALIDATION_IPV4 = 30340;
    public const VALIDATION_IPV6 = 30350;
    public const VALIDATION_JSON = 30360;
    public const VALIDATION_LT_NUMERIC = 30371;
    public const VALIDATION_LT_FILE = 30372;
    public const VALIDATION_LT_STRING = 30373;
    public const VALIDATION_LT_ARRAY = 30374;
    public const VALIDATION_LTE_NUMERIC = 30381;
    public const VALIDATION_LTE_FILE = 30382;
    public const VALIDATION_LTE_STRING = 30383;
    public const VALIDATION_LTE_ARRAY = 30384;
    public const VALIDATION_MAX_NUMERIC = 30391;
    public const VALIDATION_MAX_FILE = 30392;
    public const VALIDATION_MAX_STRING = 30393;
    public const VALIDATION_MAX_ARRAY = 30394;
    public const VALIDATION_MIMES = 30400;
    public const VALIDATION_MIMETYPES = 30410;
    public const VALIDATION_MIN_NUMERIC = 30421;
    public const VALIDATION_MIN_FILE = 30422;
    public const VALIDATION_MIN_STRING = 30423;
    public const VALIDATION_MIN_ARRAY = 30424;
    public const VALIDATION_NOT_IN = 30430;
    public const VALIDATION_NOT_REGEX = 30440;
    public const VALIDATION_NUMERIC = 30450;
    public const VALIDATION_PASSWORD = 30460;
    public const VALIDATION_PRESENT = 30470;
    public const VALIDATION_REGEX = 30480;
    public const VALIDATION_REQUIRED = 30490;
    public const VALIDATION_REQUIRED_IF = 30500;
    public const VALIDATION_REQUIRED_UNLESS = 30510;
    public const VALIDATION_REQUIRED_WITH = 30520;
    public const VALIDATION_REQUIRED_WITH_ALL = 30530;
    public const VALIDATION_REQUIRED_WITHOUT = 30540;
    public const VALIDATION_REQUIRED_WITHOUT_ALL = 30550;
    public const VALIDATION_SAME = 30560;
    public const VALIDATION_SIZE_NUMERIC = 30571;
    public const VALIDATION_SIZE_FILE = 30572;
    public const VALIDATION_SIZE_STRING = 30573;
    public const VALIDATION_SIZE_ARRAY = 30574;
    public const VALIDATION_STARTS_WITH = 30580;
    public const VALIDATION_STRING = 30590;
    public const VALIDATION_TIMEZONE = 30600;
    public const VALIDATION_UNIQUE = 30610;
    public const VALIDATION_UPLOADED = 30620;
    public const VALIDATION_URL = 30630;
    public const VALIDATION_UUID = 30640;

    public static function message(int $code, ?string $fallback = null, array $replace = []): string
    {
        $langPrefix = 'coreError::error.';
        $langCode = $langPrefix . $code;
        $msg = __($langCode, $replace);
        return $msg === $langCode
            ? $fallback ?? __($langPrefix . 'fallback', ['code' => $code])
            : $msg;
    }
}
