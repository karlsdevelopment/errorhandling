<?php

namespace Karls\ErrorHandling\Exceptions;

use App\Exceptions\Handler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Throwable;

class ExceptionHandler extends Handler
{
    public function render($request, Throwable $e): JsonResponse
    {
        if ($e instanceof ValidationException) {
            $e = new ValidatorException($e->validator);
        } elseif (!($e instanceof CoreException)) {
            $e = new CoreException(
                code: ErrorCode::UNKNOWN,
                level: CoreException::CRITICAL,
                details: [],
                extra: [
                    'location' => $e->getFile() . ': ' . $e->getLine(),
                    'prevMsg' => $e->getPrevious()?->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ],
                message: $e->getMessage(),
                previous: $e
            );
        }

        return parent::render($request, $e);
    }
}
