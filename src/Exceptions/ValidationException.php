<?php


namespace Karls\ErrorHandling\Exceptions;

class ValidationException extends CoreException
{
    public function __construct(int $code, string $name, array $details = [], array $replace = [])
    {
        $details = [$name => [
                'code' => $code,
                'message' => ErrorCode::message($code, replace: $replace),
            ] + $details
        ];

        parent::__construct(
            ErrorCode::VALIDATION_FAILED,
            CoreException::INFO,
            $details,
            $details,
        );
    }
}
