<?php


namespace Karls\ErrorHandling\Exceptions;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

class ValidatorException extends CoreException
{
    // besser direkt aus Validator, aber protected
    protected const SIZE_RULES = ['Size', 'Between', 'Min', 'Max', 'Gt', 'Lt', 'Gte', 'Lte'];
    protected const NUMERIC_RULES = ['Numeric', 'Integer'];

    public function __construct(Validator $validator)
    {
        $details = self::generateDetails($validator);
        parent::__construct(
            ErrorCode::VALIDATION_FAILED,
            CoreException::INFO,
            $details,
            $details,
        );
    }

    public static function generateDetails(Validator $validator): array
    {
        $attributes = array_keys($validator->failed());
        $rules = $validator->failed();
        $messages = $validator->messages()->toArray();
        $details = [];

        foreach ($attributes as $attribute) {
            $rule = array_key_first($rules[$attribute]);
            $ruleName = Str::upper(Str::snake($rule));

            if (in_array($rule, self::SIZE_RULES)) {
                $type = Str::upper(self::getAttributeType($validator, $attribute));
                $key = sprintf('VALIDATION_%s_%s', $ruleName, $type);
            } else {
                $key = sprintf("VALIDATION_%s", $ruleName);
            }

            $error = defined("Karls\ErrorHandling\Exceptions\ErrorCode::$key")
                ? constant("Karls\ErrorHandling\Exceptions\ErrorCode::$key")
                : ErrorCode::UNKNOWN;

            $details[$attribute] = [
                'code' => $error,
                'message' => $messages[$attribute][0],
            ];
        }

        return $details;
    }

    protected static function getAttributeType(Validator $validator, string $attribute): string
    {
        if ($validator->hasRule($attribute, self::NUMERIC_RULES)) {
            return 'numeric';
        } elseif ($validator->hasRule($attribute, ['Array'])) {
            return 'array';
        } elseif (Arr::get($validator->getData(), $attribute) instanceof UploadedFile) {
            return 'file';
        }

        return 'string';
    }
}
