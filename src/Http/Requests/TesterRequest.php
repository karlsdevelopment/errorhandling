<?php

namespace Karls\ErrorHandling\Http\Requests;


use JetBrains\PhpStorm\ArrayShape;
use Karls\Core\Http\Requests\CoreRequest;

class TesterRequest extends CoreRequest
{
    public function authorize(): bool
    {
        return true;
    }

    #[ArrayShape(['id' => "string[]"])]
    public function rules(): array
    {
        return [
            'id' => ['required']
        ];
    }
}
