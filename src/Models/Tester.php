<?php

namespace Karls\ErrorHandling\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Karls\Core\Models\CoreModel;

class Tester extends CoreModel
{
    use HasFactory;

    protected $fillable = [
        'id'
    ];
}
