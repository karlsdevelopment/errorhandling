<?php

namespace Karls\ErrorHandling;

use Illuminate\Contracts\Debug\ExceptionHandler as LaravelExceptionHandler;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Karls\ErrorHandling\Exceptions\ExceptionHandler;

class ErrorHandlingServiceProvider extends ServiceProvider
{
    public array $bindings = [];

    public array $singletons = [
        LaravelExceptionHandler::class => ExceptionHandler::class,
    ];

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../resources/lang' => resource_path('lang/vendor/coreError'),
        ]);

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'coreError');

        File::exists(__DIR__ . '/helper.php')
        && require_once __DIR__ . '/helper.php';
    }
}
