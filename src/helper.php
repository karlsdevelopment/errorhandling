<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use JetBrains\PhpStorm\ArrayShape;
use Karls\ErrorHandling\Exceptions\ErrorCode;

#[ArrayShape(['code' => "int", 'message' => "null|string", 'details' => "array|null"])]
function errorJson(
    int $code = ErrorCode::HTTP_BAD_REQUEST,
    string $message = null,
    array $details = null
): array {
    return [
        'code' => $code,
        'message' => $message,
        'details' => $details
    ];
}

function errorResponse(
    int $code = ErrorCode::HTTP_BAD_REQUEST,
    string $message = null,
    array $details = null
): JsonResponse {
    return response()->json(
        errorJson($code, $message, $details),
        Config::get('core.httpDefaultFail')
    );
}

function errorCodeMessage(int $code, array $replace = [], ?string $translationKey = null): array
{
    return [
        'code' => $code,
        'message' => $translationKey ? __($translationKey, $replace)
            : ErrorCode::message($code, replace: $replace),
    ];
}
